* General

| Key-Binding | Function Name     |
|-------------+-------------------|
| C-g         | keyboard-quit     |
| C-x b       | ivy-switch-buffer |
| C-x C-b     | list-buffers      |
| C-x C-f     | counsel-find-file |
| C-x C-s     | save-buffer       |
| C-x k       | kill-buffer       |
| C-k         | kill-visual-line  |

** Movement
| Key-Binding | Function Name       |
|-------------+---------------------+
| C-a         | beginning-of-line   |
| C-e         | end-of-line         |
| C-f         | forward-char        |
| C-b         | backward-char       |
| M-f         | forward-word        |
| M-b         | backward-word       |
| C-r         | isearch-backward    |
| M-<         | beginning-of-buffer |
| M->         | end-of-buffer       |
| M-g g       | goto-line           |

** Cut-Copy-Paste
| Key-Binding | Function Name    | Description            |
|-------------+------------------+------------------------|
| C-w         | kill-region      | Cut                    |
| M-w         | kill-ring-save   | Copy                   |
| C-y         | yank             | Paste                  |
| M-y         | counsel-yank-pop | Paste older selections |

Cmd-c - Mac copy
Cmd-v - Mac paste

** Windows
| Key-Binding | Function Name        |
|-------------+----------------------|
| C-x o       | other-window         |
| C-x 0       | delete-window        |
| C-x 1       | delete-other-windows |
| C-x 2       | split-window-below   |
| C-x 3       | split-window-right   |

** Help
| Key-Binding | Function Name      |
|-------------+--------------------|
| C-h f       | helpful-callable   |
| C-h k       | helpful-key        |
| C-h v       | helpful-variable   |
| C-h t       | help-with-tutorial |
| C-h i       | info               |

M-x menu-bar-mode

* Packages
** Undo Changes
| Key-Binding | Function Name |
|-------------+---------------|
| C-z         | undo          |
| C-x u       | undo-tree     |

Cmd-z - Mac undo
** Select text
| Key-Binding | Function Name |
|-------------+---------------|
| C-=         | expand-region |

C-Enter <Navigate> Cmd-c

** Searching
*** Searching for a file
| Key-Binding | Function Name        |
|-------------+----------------------|
| s-t         | projectile-find-file |

*** Search within a file
| Key-Binding | Function Name |
|-------------+---------------|
| C-s         | swiper        |

*** git grep
| Key-Binding | Function Name      |
|-------------+--------------------|
| C-c j       | counsel-git-grep   |
| C-x C-g     | deadgrep           |
| C-c g       | projectile-ripgrep |

*** google
| Key-Binding | Function Name |
|-------------+---------------|
| C-c / t     | google-this   |

** Multiple Cursors
| Key-Binding | Function Name           |
|-------------+-------------------------|
| C->         | mark-next-like-this     |
| C-<         | mark-previous-like-this |
| C-c C-a     | mark-all-like-this      |

** Neotree
| Key-Binding | Function Name       |
|-------------+---------------------|
| f8          | neotree-project-dir |

** Projectile
| Key-Binding     | Function Name        |
|-----------------+----------------------|
| C-c p           | projectile-mini-map  |
| Cmd-t (C-c p f) | projectile-find-file |

* Git
** Magit
| Key-Binding | Function Name        |
|-------------+----------------------|
| C-x g       | magit-status         |
| ?           | magit-dispatch       |
| Dollar($)   | magit-process-buffer |
| s           | magit-stage          |
| u           | magit-unstage        |
| F u         | magit-pull           |
| P u         | magit-push           |
| z s         | magit-stash          |
| b           | magit-branch         |
| m           | magit-merge          |

Git Blame
  M-x magit-blame-addition

** Git-TimeMachine
Timetravel through the current buffer
  M-x git-timemachine

* Clojure
** Paredit

http://danmidwood.com/content/2014/11/21/animated-paredit.html

https://github.com/joelittlejohn/paredit-cheatsheet

** Cider
| Key-Binding | Function Name                      | Description                                        |
|-------------+------------------------------------+----------------------------------------------------|
| C-c Cmd-c   | cider-connect-clj                  | cider-connect                                      |
| C-c C-k     | cider-load-buffer                  | eval and load namespace                            |
| C-c C-z     | cider-switch-to-repl-buffer        | moving between repl and buffer                     |
| C-c M-n n   | cider-repl-set-ns                  | set repl to current namespace                      |
| C-u C-c C-z | cider-repl-set-ns                  | switch to repl and set namespace to current buffer |
| Cmd-k       | cider-repl-clear-buffer            | clear repl buffer                                  |
| C-c C-q     | cider-quit                         | quit repl                                          |
| M-.         | cider-find-var                     | go to source from symbol at point                  |
| M-,         | cider-pop-back                     | go to documentation from source                    |
| C-x C-e     | cider-eval-last-sexp               | eval last sexp                                     |
| C-c C-c     | cider-eval-defun-at-point          | eval top level sexp                                |
| C-c C-d C-d | cider-doc                          | go to documentation from symbol at point           |
| C-c C-d a   | cider-apropos                      | appropos-search (among things loaded in the repl)  |
| C-c C-d e   | cider-apropos-documentation-select | search for regex in docstrings of function symbols |
| C-c C-d C-j | cider-javadoc                      | java docs in browser                               |
| C-c M-n M-b | cider-browse-namespace             | to browse all namescpaces(including dependencies)  |

*** Testing
| Key-Binding | Function Name           |
|-------------+-------------------------|
| C-c C-t n   | cider-test-run-ns-tests |

or switch to test namespace in repl and call the test expression

*** Cider-repl

| Key-Binding | Function Name             |
|-------------+---------------------------|
| C-shift-↑   | cider-repl-backward-input |
| C-shift-↓   | cider-repl-forward-input  |

C-↑, C-↓	Cycle through REPL history.
Note for Mac users: by default, OS X maps C-↑, C-↓, C-←, and C-→ to Mission Control commands.

**** errors

***** Close
C-x o -> q

***** View
Go to *cider-error* bufferor Enter *e in the repl buffer

*** Debugging

https://docs.cider.mx/cider/debugging/tracing.html

C-c M-t v - (enter name and invoice of a call)

M-x Cider-inspect-element - to inspect data-structures with nestedness

C-u M-c M-c - interactive debugger
q- quit the debugger ,
S- see stacktraces till then (filter stacktraces)
re-evaluate expression to remove it from debugging.

M-x cider-enlighten-mode - to show intermediate expressions

M-x customize-group-cider

* Resources

https://thb.lt/emacs-cheatsheet/

https://www.braveclojure.com/basic-emacs/

** Packages
*** Ivy, Counsel, Swiper
https://sam217pa.github.io/2016/09/13/from-helm-to-ivy/
https://writequit.org/denver-emacs/presentations/2017-04-11-ivy.html
http://blog.binchen.org/posts/hello-ivy-mode-bye-helm.html
https://oremacs.com/swiper/

*** Cider
 https://www.youtube.com/watch?v=aYA4AAjLfT0
 https://cider.mx/

*** Magit
 https://www.youtube.com/watch?v=OMIxZhLU71U
 https://magit.vc/
